# install_jmodelica

This is an installation script for [JModelica](https://jmodelica.org/) under
Ubuntu Linux. The script is based on original code taken from the [JModelica
User's Guide](https://jmodelica.org/downloads/UsersGuide-2.10.pdf) and a
[contribution at
Stackoverflow](https://stackoverflow.com/questions/55230042/jmodelica-on-ubuntu-18-04).

JModelica is based on Python 2.7 and Java 8.

Currently, the installation script supports

- Ubuntu Linux 18.04
- Linux Mint 19

# Start, compilation, simulation

Start `jmodelica.sh` from bash:

```python
from pyfmi import load_fmu
from pymodelica import compile_fmu
from pylab import *

fmu = compile_fmu('Modelica.Blocks.Examples.Filter',version = '2.0', target = 'me+cs', compiler = 'modelica', separate_process = False)
model = load_fmu(fmu)
opts = model.simulate_options()
opts["ncp"] = 10000
res = model.simulate(final_time=1,options=opts)
```
